/* eslint require-jsdoc:0 */
import gulp from 'gulp';
import * as config from './config';
import * as util from './util.babel';
import eslint from 'gulp-eslint';
import browserSyncProgram from 'browser-sync';
export const reload = browserSyncProgram.reload;

export function lint() {
  return gulp.src(config.JAVASCRIPTSOURCES)
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
}

export function css() {
  const stream = gulp.src('resources/styles/app.css');
  return util.cssPipeline(stream);
}

export function javascript() {
  const stream = gulp.src(config.JAVASCRIPTSOURCES);
  return util.jsPipeline(stream);
}

export function pages() {
  const stream = gulp.src('resources/pages/*.twig');
  return util.pagePipeline(stream);
}

export function browserSync() {
  browserSyncProgram({
    server: {
      baseDir: './public'
    },
    notify: false
  });
}
