export const JAVASCRIPTSOURCES = [
  'resources/scripts/components/*.js'
];

export const STYLESOURCES = [
  'resources/styles/**/*.css'
];

export const MANIFESTFILE = 'public/asset-revision-manifest.json';

export const MANIFESTCONFIG = {
  merge: true,
  base: './'
};

export const TWIGOPTIONS = {
  functions: [
    {
      name: 'asset',
      func: function(lookup) {
        const revisions = require('../public/asset-revision-manifest.json');
        for (let key in revisions) {
          if (key.trim() === lookup.trim()) {
            return `a/${revisions[key]}`;
          }
        }
        throw new Error('Asset not found');
      }
    }
  ]
};
