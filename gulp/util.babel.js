/* eslint require-jsdoc:0 */
import gulp from 'gulp';
import * as config from './config';
import bemLinter from 'postcss-bem-linter';
import cssImport from 'postcss-import';
import postCssReporter from 'postcss-reporter';
import customProperties from 'postcss-custom-properties';
import customMedia from 'postcss-custom-media';
import nano from 'gulp-cssnano';
import sourcemaps from 'gulp-sourcemaps';
import postcss from 'gulp-postcss';
import rename from 'gulp-rename';
import rev from 'gulp-rev';
import twig from 'gulp-twig';

export function cssPipeline(stream, dest = 'public/a') {
  return stream
    .pipe(sourcemaps.init())
    .pipe(postcss([
      cssImport,
      customProperties,
      customMedia,
      postCssReporter({
        clearMessages: true
      })
    ]))
      .pipe(postcss([
        bemLinter({
          preset: 'bem',
          presetOptions: {
            namespace: 'app'
          },
          componentName: /[a-z -]/,
          ignoreSelectors: [
            new RegExp('.material-icons'),
            /\*/,
            /fieldset\[disabled\]/
          ]
        }),
        postCssReporter({
          clearMessages: true
        })
      ]))
    .pipe(nano({
      autoprefixer: false,
      colormin: true,
      convertValues: {
        length: false,
        time: true,
        angle: false
      },
      discardComments: {
        removeAll: true
      },
      discardDuplicates: true,
      discardEmpty: true,
      discardUnused: {
        keyframes: true,
        fontFace: true,
        counterStyle: true
      },
      mergeIdents: true,
      mergeLonghand: true,
      mergeRules: true,
      minifyFontValues: {
        removeAfterKeyword: true,
        removeDuplicates: true,
        removeQuotes: false
      },
      minifyGradients: true,
      minifySelectors: true,
      normalizeCharset: {
        add: true
      },
      orderedValues: true,
      reduceIndents: {
        counter: false,
        keyframes: false,
        counterStyle: false
      },
      reduceTransforms: true,
      uniqueSelectors: true,
      zindex: false
    }))
    .pipe(rename({extname: '.min.css'}))
    .pipe(rev())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dest))
    .pipe(rev.manifest(config.MANIFESTFILE, config.MANIFESTCONFIG))
    .pipe(gulp.dest('./'));
}

export function jsPipeline(stream, dest = 'public/a') {
  return stream
    .pipe(sourcemaps.init())
    .pipe(rev())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dest))
    .pipe(rev.manifest(config.MANIFESTFILE, config.MANIFESTCONFIG))
    .pipe(gulp.dest('./'));
}

export function pagePipeline(stream, dest = 'public') {
  return stream
    .pipe(twig(config.TWIGOPTIONS))
    .pipe(rename({extname: '.html'}))
    .pipe(gulp.dest(dest));
}
