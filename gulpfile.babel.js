import gulp from 'gulp';
import * as tasks from './gulp/tasks.babel';
import * as config from './gulp/config';

gulp.task('styles', gulp.parallel(
  tasks.css
));

gulp.task('scripts', gulp.series(
  tasks.lint,
  tasks.javascript
));

gulp.task('pages', gulp.series(
  gulp.parallel('styles', 'scripts'),
  tasks.pages
));

gulp.task('test', gulp.series(
  gulp.parallel('scripts')
));

gulp.task('default', gulp.series(
  'pages'
));

gulp.task('watch:css',
  () => gulp.watch(config.STYLESOURCES,
    gulp.series('pages', tasks.reload)));

gulp.task('watch:scripts',
  () => gulp.watch(config.JAVASCRIPTSOURCES,
    gulp.series('pages', tasks.reload)));

gulp.task('watch:pages',
  () => gulp.watch('resources/pages/*.twig',
    gulp.series('pages', tasks.reload)));

gulp.task('watch', gulp.parallel(
  'watch:css',
  'watch:scripts',
  'watch:pages'
));

gulp.task('serve', gulp.parallel(
  'watch',
  tasks.browserSync
));
